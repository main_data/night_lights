# Autor(es): Eduard Martínez
# Fecha última modificación: 
# Versión de R: 4.0.3.

#--- limpiar entorno y cargar paquetes ---#
rm(list=ls())
require(pacman)
p_load(raster , tidyverse , sf , rio , data.table , leaflet , stars)

##=================##
## 1. Spatial join ## 
##=================##

#--- function that crop raster and change to sf ---#
f_crop <- function(raster,mpio,name,code){
          print(code)
          mpio_i <- subset(mpio , cod_mpio==code)
          
          raster_i <- raster %>% st_crop(mpio_i) %>% .[mpio_i,] %>% mutate(cod_mpio=code)  
          
export(raster_i,paste0('4_joint_raster_mpio/output/temp/',name,code,'.rds'))
}

vector <- c(1,seq(50,1100,50),1119)
for (i in 1:(length(vector)-1)) {
     codes <- import("4_joint_raster_mpio/input/codes_tile_1.rds") %>% 
              sort() %>% .[vector[i]:vector[i+1]]
    
     mpio <- import("4_joint_raster_mpio/input/layers/dp mpios (2017).rds") %>% 
             subset(cod_mpio %in% codes)
    
     viirs <- import('3_raster_to_sf/output/poly_harmonized_2008-2011.rds') %>% .[mpio,]
    
     lapply(codes, function(i) f_crop(raster=viirs , mpio=mpio , name='harmonized_1_year' , code=i))
}
for (i in 1:(length(vector)-1)) {
     codes <- import("4_joint_raster_mpio/input/codes_tile_1.rds") %>% 
              sort() %>% .[vector[i]:vector[i+1]]
    
     mpio <- import("4_joint_raster_mpio/input/layers/dp mpios (2017).rds") %>% 
             subset(cod_mpio %in% codes)
    
     viirs <- import('3_raster_to_sf/output/poly_harmonized_1992-2007_2012-2021.rds') %>% .[mpio,]
    
     lapply(codes, function(i) f_crop(raster=viirs , mpio=mpio , name='harmonized_2_year' , code=i))
}

##=============================##
## 2. Add Urban and Rural Area ## 
##=============================##

#--- make function crop raster to sf ---#
f_urban <- function(urban,file,name){
  
           ## load data
           print(file)
           mpio_i <- import(file = file)
           code = file %>% 
                  gsub("4_joint_raster_mpio/output/temp/harmonized_1_year","",.) %>%
                  gsub("4_joint_raster_mpio/output/temp/harmonized_2_year","",.) %>%
                  gsub(".rds","",.) %>% as.numeric()
           print(code)
             
           ## subset data
           kapital_i <- urban %>% subset(cod_mpio==code) %>% subset(kapital==1) %>% select(kapital)
           rur_cp_i <- urban %>% subset(cod_mpio==code) %>% subset(rural_cp==1) %>% select(rural_cp)
           
           ## st join
           mpio_i <- st_join(mpio_i,kapital_i)
           if(nrow(rur_cp_i)>0){mpio_i <- st_join(mpio_i,rur_cp_i)}
           
export(mpio_i,paste0('4_joint_raster_mpio/output/temp/',name,code,'.rds'))
}

## get files 
files_1 <- list.files(path = "4_joint_raster_mpio/output/temp"  , full.names = T)  %>% str_subset("temp/harmonized_1_year")
files_2 <- list.files(path = "4_joint_raster_mpio/output/temp"  , full.names = T)  %>% str_subset("temp/harmonized_2_year")

## define urban layer
urban <- st_read("4_joint_raster_mpio/input/layers/MGN_URB_SECCION.shp") %>%
         mutate(kapital=ifelse(substr(COD_CPOB,6,8)=="000",1,0),
                rural_cp=ifelse(substr(COD_CPOB,6,8)!="000",1,0),
                cod_mpio=as.numeric(COD_MPIO))
urban <- st_transform(urban,
                      st_crs(import(files_1[1])))

## apply function
lapply(files_1, function(i) f_urban(urban=urban , file=i , name='sf_urban_harmo_1_'))

lapply(files_2, function(i) f_urban(urban=urban , file=i , name='sf_urban_harmo_2_'))

## plot map
file = "4_joint_raster_mpio/output/temp/harmonized_1_year52001.rds"
mpio_i <- import(file = file)
kapital_i <- urban %>% subset(cod_mpio==52001) %>% subset(kapital==1)
rur_cp_i <- urban %>% subset(cod_mpio==52001) %>% subset(rural_cp==1)

leaflet() %>% addTiles() %>% 
addPolygons(data=mpio_i , fill=NA , color="blue") %>%
addPolygons(data=kapital_i , col="yellow") %>% 
addPolygons(data=rur_cp_i , col="red")


##==============##
## 2. Bind Data ## 
##==============##

#--- limpiar entorno ---#
rm(list=ls())

#--- load files ----#
files <- list.files(path="4_joint_raster_mpio/output/temp", full.names=T) %>% str_subset("sf_urban_harmo_1_")

#--- stack data ---# 
f_read <- function(x){df <- import(x) ; st_geometry(df) = NULL ; return(df)}
db <- lapply(files , function(x) f_read(x=x)) %>% rbindlist(use.names=T , fill=T)

#--- export data ---# 
export(db , "4_joint_raster_mpio/output/df_grid-mpio_harmo-year_2008-2011.dta")

#--- limpiar entorno ---#
rm(list=ls())

#--- load files ----#
files <- list.files(path="4_joint_raster_mpio/output/temp", full.names=T) %>% str_subset("sf_urban_harmo_2_")

#--- stack data ---# 
f_read <- function(x){df <- import(x) ; st_geometry(df) = NULL ; return(df)}
db <- lapply(files , function(x) f_read(x=x)) %>% rbindlist(use.names=T , fill=T)

#--- export data ---# 
export(db , "4_joint_raster_mpio/output/df_grid-mpio_harmo-year_1992-2007_2012-2021.dta")





















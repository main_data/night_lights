#==============================================================================#
# Autor(es): Eduard Martínez
# Colaboradores: 
# Fecha creación: 
# Fecha última modificación: 
# Versión de R: 4.0.3.
#==============================================================================#

#--- limpiar entorno y cargar paquetes ---#
rm(list=ls())
require(pacman)
p_load(here , tidyverse , sf , raster)
path = here('')

#--- load data ---#
viirs = list.files(paste0(path,'1_download/output/viirs'),full.names = T)
dmsp = list.files(paste0(path,'1_download/output/dmsp'),full.names = T)
country = readRDS(paste0(path,'1_download/input/layers/colombia disolve (sin sa).rds'))

#--- apilando datos de viirs ---#
files = viirs
country = st_transform(x=country , crs=crs(raster(x = files[1])))
stack_raster = map(files, function(x) raster(x=x)) %>% stack() 
names(stack_raster) = gsub('colombia_','month_',names(stack_raster))
saveRDS(stack_raster, paste0(path,'2_stack_raster/output/rasters_viirs_201204_202112.rds'))
writeRaster(stack_raster, paste0(path,'2_stack_raster/output/rasters_viirs_201204_202112.tif'))

#--- apilando datos de dmsp ---#
files = dmsp
country = st_transform(x=country , crs=crs(raster(x = files[1])))
stack_raster = lapply(files, function(y) raster(x = y)) %>% stack() 
names(stack_raster) = gsub('colombia_','year_',names(stack_raster))
saveRDS(stack_raster, paste0(path,'2_stack_raster/output/rasters_dmsp_1992_2013.rds'))

#--- apilando datos de harmonized ---#
lista_raster = readRDS(paste0(path,'1_download/output/harmonized/lista_rasters_harmonized_1992_2021.rds'))
lapply(lista_raster, function(y) extent(y)) # ver cuales raster tienen diferente extent
stack_raster_1 = lista_raster[c(1:17,21:30)] %>% stack() 
stack_raster_2 = lista_raster[18:20] %>% stack() 
names(stack_raster_1) = gsub("Harmonized_DN_NTL_","",names(stack_raster_1)) %>% substr(.,1,4) %>% paste0("year_",.)
names(stack_raster_2) = gsub("Harmonized_DN_NTL_","",names(stack_raster_2)) %>% substr(.,1,4) %>% paste0("year_",.)
stack_raster = list("raster_2008_2011" = stack_raster_2 , "raster_1992_2021" = stack_raster_1)
saveRDS(stack_raster, paste0(path,'2_stack_raster/output/rasters_harmonized_1992_2021.rds'))


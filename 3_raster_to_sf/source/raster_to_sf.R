#==============================================================================#
# Autor(es): Eduard Martinez
# Update: 09/09/2022  
# Versión de R: 4.1.2
#==============================================================================#

## initial configuration
rm(list=ls())
require(pacman)
p_load(raster,tidyverse,sf,stars,leaflet,rio)

##=== Datos mensuales ===##
## raster to sf
n_light <- read_stars("1_import_data/input/night_light/input/rasters_viirs_201204_202112.tif") %>%
           st_as_sf(as.points=T)

## rename vars
names <- rep(2012:2021,12) %>% sort() %>% paste0(.,c(paste0("0",1:9),10:12)) %>% .[4:120] %>% paste0("m",.)
colnames(n_light)[1:117] <- names

## export data
export(n_light,"1_import_data/input/night_light/output/poly_viirs_201204_202112.rds")

##=== Datos anueales ===##

## raster to sf
n_light <- import("2_stack_raster/output/rasters_harmonized_1992_2021.rds") %>% 
           .[["raster_2008_2011"]] %>% rasterToPolygons() %>%
           st_as_sf()

## export data
export(n_light,"3_raster_to_sf/output/poly_harmonized_2008-2011.rds")

## raster to sf
n_light <- import("2_stack_raster/output/rasters_harmonized_1992_2021.rds") %>% 
           .[["raster_1992_2021"]] %>% rasterToPolygons() %>%
           st_as_sf()

## export data
export(n_light,"3_raster_to_sf/output/poly_harmonized_1992-2007_2012-2021.rds")





